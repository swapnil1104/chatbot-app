package com.broooapps.chatbot.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.broooapps.chatbot.models.User;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
public class UserRepository {

    private static UserRepository repository;
    private MutableLiveData<User> userLiveData;

    private UserRepository() {
        userLiveData = new MutableLiveData<>();

        User user = new User("Swapnil", "Tiwari", "m");
        userLiveData.setValue(user);
    }


    public static UserRepository getInstance() {
        if (repository == null) {
            repository = new UserRepository();
        }
        return repository;
    }

    public LiveData<User> getUser() {
        return userLiveData;
    }

}
