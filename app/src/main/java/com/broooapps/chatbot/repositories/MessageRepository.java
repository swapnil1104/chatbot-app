package com.broooapps.chatbot.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Room;

import com.broooapps.chatbot.models.ApiResponse;
import com.broooapps.chatbot.models.Message;
import com.broooapps.chatbot.networking.OkHttpTask;
import com.broooapps.chatbot.utils.AppDatabase;
import com.broooapps.chatbot.utils.Constants;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
public class MessageRepository {
    // static repo instance for singleton impl
    private static MessageRepository repository;

    // room db obj
    private AppDatabase database;

    // livedata objs
    private MutableLiveData<Message> messageMutableLiveData;
    private MutableLiveData<List<Message>> listMessageMutableLiveData;

    // current chat id value holder.
    private int curChatId;

    private MessageRepository(Application application) {
        WeakReference<Application> weakReference = new WeakReference<>(application);
        messageMutableLiveData = new MutableLiveData<>();
        listMessageMutableLiveData = new MutableLiveData<>();

        database = Room.databaseBuilder(weakReference.get(), AppDatabase.class, "message_db")
                .fallbackToDestructiveMigration()
                .build();

        pullDataFromStorage(1);
    }

    public static MessageRepository getInstance(Application application) {
        if (repository == null) {
            repository = new MessageRepository(application);
        }
        return repository;
    }


    /* ---------------------------------------- Getter methods ---------------------------------------- */
    public LiveData<List<Message>> getPrevMessagesLiveData() {
        return listMessageMutableLiveData;
    }

    public LiveData<Message> getMessageLiveData() {
        return messageMutableLiveData;
    }
    /* ------------------------------------------------------------------------------------------ */

    /**
     * Get all the chat messages from storage for the param id
     *
     * @param id :: the chat id.
     */
    public void pullDataFromStorage(int id) {
        new Thread(() -> {
            List<Message> messages = database.messageDao().loadMessageByChatId(id);
            listMessageMutableLiveData.postValue(messages);
        }).start();
    }

    /**
     * Create a network call using OkHttpClient instance and send it over the network.
     * it also performs the process of storing the message object in local storage.
     *
     * @param message
     */
    public void sendMessage(Message message) {

        try {
            storeMsgInDb(message);
        } catch (Exception e) {
            new Thread(() -> database.messageDao().update(message)).start();
        }

        final OkHttpClient client = OkHttpTask.getClient();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.HOST_URL).newBuilder();
        urlBuilder.addQueryParameter("apiKey", Constants.API_KEY);
        urlBuilder.addQueryParameter("message", message.getMessage());
        urlBuilder.addQueryParameter("chatBotID", String.valueOf(Constants.CHAT_BOT_ID));
        urlBuilder.addQueryParameter("externalID", generateExternalID(message));
        urlBuilder.addQueryParameter("firstName", message.getUser().getFirstName());
        urlBuilder.addQueryParameter("lastName", message.getUser().getLastName());
        urlBuilder.addQueryParameter("gender", message.getUser().getGender());
        curChatId = message.getChatId();
        String url = urlBuilder.build().toString();
        final Request request = new Request.Builder()
                .url(url)
                .build();

        new Thread(() -> {
            try {
                Response response = client.newCall(request).execute();

                String res = response.body().string();
                response.close();
                ApiResponse apiResponse = new Gson().fromJson(res, ApiResponse.class);

                apiResponse.getMessage().setFromUser(false);
                apiResponse.getMessage().setChatId(curChatId);

                storeMsgInDb(apiResponse.getMessage());

                messageMutableLiveData.postValue(apiResponse.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

    }

    /**
     * Method to generate the unique external id for a message.
     *
     * @param message :: message obj.
     * @return :: generated id
     */
    private String generateExternalID(Message message) {
        return message.getUser().getFirstName() + message.getUser().getLastName();
    }


    /**
     * Util method to delete all records stored in the db.
     */
    public void deleteAllData() {
        new Thread(() -> {
            database.messageDao().deleteAll();
        }).start();
    }

    /**
     * Util method to store the message object in db.
     *
     * @param message
     */
    public void storeMsgInDb(Message message) {
        new Thread(() -> {
            database.messageDao().insert(message);
        }).start();

    }

    /**
     * Util method to find out the messages that are pending and then send them over the netowrk
     * once the internet is available again.
     */
    public void sendPendingMessages() {
        new Thread(() -> {
            List<Message> messsages = database.messageDao().getPendingMessage();

            for (Message message : messsages) {
                message.setPending(false);
                sendMessage(message);
            }

        }).start();
    }
}