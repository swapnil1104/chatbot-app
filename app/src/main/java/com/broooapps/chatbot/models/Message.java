package com.broooapps.chatbot.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
@Entity
public class Message {
    @PrimaryKey(autoGenerate = true)
    public int msgId;

    @ColumnInfo
    public
    boolean fromUser;

    @ColumnInfo
    private
    int chatBotID;

    @ColumnInfo
    private
    String message;

    @ColumnInfo
    private
    String emotion;

    @Embedded
    private
    User user;

    @ColumnInfo
    private
    int chatId;

    @ColumnInfo
    private
    boolean pending;

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isFromUser() {
        return fromUser;
    }

    public void setFromUser(boolean fromUser) {
        this.fromUser = fromUser;
    }

    public int getChatBotID() {
        return chatBotID;
    }

    public void setChatBotID(int chatBotID) {
        this.chatBotID = chatBotID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }
}
