package com.broooapps.chatbot.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.broooapps.chatbot.models.Message;
import com.broooapps.chatbot.models.User;
import com.broooapps.chatbot.repositories.MessageRepository;
import com.broooapps.chatbot.repositories.UserRepository;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
public class ChatActivityViewModel extends AndroidViewModel {

    // repository objects
    private MessageRepository messageRepository;
    private UserRepository userRepository;

    // live data objects
    private LiveData<User> userLiveData;
    private LiveData<Message> messageLiveData;
    private LiveData<List<Message>> prevMessages;

    public ChatActivityViewModel(@NonNull Application application) {
        super(application);

        userRepository = UserRepository.getInstance();
        messageRepository = MessageRepository.getInstance(getApplication());


        userLiveData = userRepository.getUser();
        messageLiveData = messageRepository.getMessageLiveData();
        prevMessages = messageRepository.getPrevMessagesLiveData();
    }

    /* ---------------------------------------- Getter methods ---------------------------------------- */
    public LiveData<List<Message>> getPrevMessages() {
        return prevMessages;
    }

    public LiveData<Message> getMessageLiveData() {
        return messageLiveData;
    }

    public User getUser() {
        return userLiveData.getValue();
    }
    /* ----------------------------------------------------------------------------------------------------*/

    /**
     * This method notifies the repository to fetch the chat data for specific chat id.
     *
     * @param id :: chat id.
     */
    public void pullDataFromStorage(int id) {
        messageRepository.pullDataFromStorage(id);
    }

    /**
     * This method tells the repository whether the message should be sent now, or else should be stored
     * to be sent later.
     *
     * @param message :: msg obj
     */
    public void sendMessage(Message message) {
        if (isNetworkAvailable()) {
            messageRepository.sendMessage(message);
        } else {
            message.setPending(true);
            messageRepository.storeMsgInDb(message);
        }
    }

    /**
     * Method to delete all stored data.
     */
    public void deleteAllData() {
        messageRepository.deleteAllData();
    }

    /**
     * this method notifies the repository to send all the pending messages, once the netowrk is avialable.
     *
     * @param id :: chat Id.
     */
    public void sendPendingMessages(int id) {
        messageRepository.sendPendingMessages();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                messageRepository.pullDataFromStorage(id);
            }
        }, 1000);
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
