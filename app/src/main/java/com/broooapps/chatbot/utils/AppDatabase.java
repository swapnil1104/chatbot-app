package com.broooapps.chatbot.utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.broooapps.chatbot.dao.MessageDao;
import com.broooapps.chatbot.models.Message;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */

@Database(entities = {Message.class}, version = 6)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MessageDao messageDao();
}
