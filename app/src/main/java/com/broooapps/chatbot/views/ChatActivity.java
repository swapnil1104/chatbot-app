package com.broooapps.chatbot.views;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.broooapps.chatbot.R;
import com.broooapps.chatbot.adapters.ChatRecyclerAdapter;
import com.broooapps.chatbot.models.Message;
import com.broooapps.chatbot.utils.Constants;
import com.broooapps.chatbot.viewmodels.ChatActivityViewModel;

public class ChatActivity extends AppCompatActivity {

    /**
     * Data objects
     */
    private ChatRecyclerAdapter chatRecyclerAdapter;
    private ChatActivityViewModel viewModel;
    ConnectionStateMonitor connectionStateMonitor;

    /**
     * View objects.
     */
    private ImageView button_send;
    private EditText edit_msg;
    private RecyclerView recycler_chat;
    private TextView text_chat_with;


    /**
     * case to handle more than 1 chat screens.
     */
    private MutableLiveData<Integer> chatIdMutableLiveData;

    /* ---------------------------------- Activity lifecycler methods ------------------------------ */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        viewModel = ViewModelProviders.of(this).get(ChatActivityViewModel.class);
        chatIdMutableLiveData = new MutableLiveData<>();
        chatIdMutableLiveData.setValue(1);
        initializeViews();
        defaultConfigurations();
        setEventsForViews();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.chat1:
                updateChatScreen(1);
                return true;

            case R.id.chat2:
                updateChatScreen(2);
                return true;

            case R.id.delete_data:
                viewModel.deleteAllData();
                updateChatScreen(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*------------------------------------------------------------------------------------------*/

    private void initializeViews() {
        button_send = findViewById(R.id.button_send);
        recycler_chat = findViewById(R.id.recycler_chat);
        edit_msg = findViewById(R.id.edit_msg);
        text_chat_with = findViewById(R.id.text_chat_with);
    }

    private void defaultConfigurations() {
        if (connectionStateMonitor == null) {
            connectionStateMonitor = new ConnectionStateMonitor();
            connectionStateMonitor.enable(this);
        }
        initializeRecyclerAdapter();

        chatIdMutableLiveData.observe(this, integer -> {
            if (integer == null) return;
            if (integer == 1) {
                text_chat_with.setText(getString(R.string.msg_chat_1));
            } else {
                text_chat_with.setText(getString(R.string.msg_chat_2));
            }
        });

        viewModel.getMessageLiveData().observe(this, message -> {
            if (message != null) {
                message.setFromUser(false);
                chatRecyclerAdapter.addMessage(message);
                recycler_chat.smoothScrollToPosition(chatRecyclerAdapter.getItemCount() - 1);
            }
        });

        viewModel.getPrevMessages().observe(this, messages -> {
            chatRecyclerAdapter.replaceMessageList(messages);
            if (chatRecyclerAdapter.getItemCount() > 0) {
                recycler_chat.smoothScrollToPosition(chatRecyclerAdapter.getItemCount() - 1);
            }
        });
    }

    private void setEventsForViews() {
        button_send.setOnClickListener(v -> {
            String msg = String.valueOf(edit_msg.getText()).trim();
            edit_msg.setText(null);

            if (msg.equalsIgnoreCase("")) {
                Toast.makeText(this, "Please enter a message first.", Toast.LENGTH_SHORT).show();
            } else {
                sendMessage(msg);
            }
        });
    }

    private void initializeRecyclerAdapter() {
        chatRecyclerAdapter = new ChatRecyclerAdapter();
        recycler_chat.setLayoutManager(new LinearLayoutManager(this));
        recycler_chat.setAdapter(chatRecyclerAdapter);

    }

    /**
     * This method is used when the chat screen has been changed. it removes all the observers,
     * and set's up the activity for the next chat id.
     *
     * @param id :: chat id.
     */
    private void updateChatScreen(int id) {
        chatIdMutableLiveData.setValue(id);
        viewModel.pullDataFromStorage(chatIdMutableLiveData.getValue());
        chatRecyclerAdapter = null;

        // removing observers.
        viewModel.getMessageLiveData().removeObservers(this);
        viewModel.getPrevMessages().removeObservers(this);

        defaultConfigurations();
    }

    /**
     * Send the message String for
     *
     * @param msg
     */
    private void sendMessage(String msg) {
        // update recycler view as well.
        Message m = generateMessage(msg);

        chatRecyclerAdapter.addMessage(m);
        recycler_chat.smoothScrollToPosition(chatRecyclerAdapter.getItemCount() - 1);
        viewModel.sendMessage(m);
    }

    /**
     * Generate message object from the msg string for further processing.
     *
     * @param msg :: text message
     * @return :: Message object
     */
    private Message generateMessage(String msg) {
        Message m = new Message();
        m.setMessage(msg);
        m.setChatBotID(Constants.CHAT_BOT_ID);
        m.setFromUser(true);
        m.setUser(viewModel.getUser());
        m.setChatId(chatIdMutableLiveData.getValue());
        return m;
    }

    /**
     * Class to observe network callbacks, and notify the activiy once network becomes available.
     */
    class ConnectionStateMonitor extends ConnectivityManager.NetworkCallback {

        final NetworkRequest networkRequest;

        public ConnectionStateMonitor() {
            networkRequest = new NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR).addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build();
        }

        public void enable(Context context) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerNetworkCallback(networkRequest, this);
        }

        // Likewise, you can have a disable method that simply calls ConnectivityManager.unregisterNetworkCallback(NetworkCallback) too.

        @Override
        public void onAvailable(Network network) {
            // Do what you need to do here
            viewModel.sendPendingMessages(chatIdMutableLiveData.getValue());
        }
    }
}
