package com.broooapps.chatbot.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.broooapps.chatbot.R;
import com.broooapps.chatbot.models.Message;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
public class ChatRecyclerAdapter extends RecyclerView.Adapter<ChatRecyclerAdapter.ChatViewHolder> {

    private ArrayList<Message> messageArrayList;

    public ChatRecyclerAdapter() {
        messageArrayList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.vh_chat_item, viewGroup, false);

        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int pos) {
        Message message = messageArrayList.get(pos);
        if (message != null) {
            if (message.fromUser) {
                chatViewHolder.text_chat_user.setVisibility(View.VISIBLE);
                chatViewHolder.text_chat_user.setText(message.getMessage());
                chatViewHolder.text_chat_bot.setVisibility(View.GONE);
                if (message.isPending()) {
                    chatViewHolder.text_chat_user.setBackgroundResource(R.drawable.light_gray_bg);

                } else {
                    chatViewHolder.text_chat_user.setBackgroundResource(R.drawable.light_green_bg);
                }
            } else {
                chatViewHolder.text_chat_bot.setVisibility(View.VISIBLE);
                chatViewHolder.text_chat_bot.setText(message.getMessage());
                chatViewHolder.text_chat_user.setVisibility(View.GONE);
            }
        }

    }

    public void addMessage(Message message) {
        messageArrayList.add(message);
        notifyItemChanged(messageArrayList.size() - 1);
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    public void replaceMessageList(List<Message> messages) {
        messageArrayList.clear();
        messageArrayList = new ArrayList<>(messages);
        notifyDataSetChanged();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder {

        TextView text_chat_bot;
        TextView text_chat_user;

        ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            text_chat_bot = itemView.findViewById(R.id.text_chat_bot);
            text_chat_user = itemView.findViewById(R.id.text_chat_user);

        }
    }
}
