package com.broooapps.chatbot.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.broooapps.chatbot.models.Message;

import java.util.List;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
@Dao
public interface MessageDao {
    @Query("SELECT * FROM message WHERE chatId = :chatId")
    List<Message> loadMessageByChatId(int chatId);

    @Insert
    void insert(Message m);

    @Update
    void update(Message m);

    @Query("DELETE FROM message")
    void deleteAll();

    @Query("SELECT * FROM message WHERE pending = 1")
    List<Message> getPendingMessage();
}