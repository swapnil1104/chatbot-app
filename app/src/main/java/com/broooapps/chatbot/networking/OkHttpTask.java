package com.broooapps.chatbot.networking;

import okhttp3.OkHttpClient;

/**
 * Created by Swapnil Tiwari on 2019-04-20.
 * swapnil.tiwari@box8.in
 */
public class OkHttpTask {
    private static OkHttpClient client;

    private OkHttpTask() {
    }

    public static OkHttpClient getClient() {
        if (client == null) {
            client = new OkHttpClient.Builder().build();
        }
        return client;
    }

}
